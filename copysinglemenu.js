const axios = require('axios')
const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')
const moment = require('moment')

let config = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    },
    production: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    }
}

let m_fromOutletName = 'Kalimaya Kuningan'
let m_toOutletName = 'Kalimaya Banten'
let m_menuName = 'Nasi Uduk Bakar'

let env = process.env.NODE_ENV || 'development'
let dbconfig = config[env]
console.log('> RUNNING IN '+env+' MODE!!')
console.log('==============================')

//const tableName = "OUTLET_COPY"
const tableName = "meeber_pos_v2_library.PRODUCT"
const conn = new Sequelize(dbconfig.database, dbconfig.user, dbconfig.password, dbconfig)

async function fetchMenu(outletName, menuName) {
    try {
        console.log('> Find menu data ' + menuName + ' from outlet ' + outletName)
        let query = "SELECT meeber_pos_v2_library.PRODUCT.* FROM " + tableName + " INNER JOIN meeber_pos_v2_auth.OUTLET ON meeber_pos_v2_auth.OUTLET.OUTLET_ID = " + tableName + ".OUTLET_ID WHERE meeber_pos_v2_auth.OUTLET.OUTLET_NAME = '" + outletName + "' AND " + tableName + ".PRODUCT_NAME = '" + menuName + "'"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function getTargetOutletData(outletName) {
    try {
        console.log('> Find outlet data ' + outletName)
        let query = "SELECT * FROM meeber_pos_v2_auth.OUTLET WHERE meeber_pos_v2_auth.OUTLET.OUTLET_NAME='" + outletName + "'"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function insertMenu(outletData, menuData) {
    let currentDate = moment().format('YYYY-MM-DD HH:mm:ss')
    let normalOutletId = uuidParse.unparse(outletData.OUTLET_ID)

    try {
        let query = 'INSERT INTO ' + tableName
        query += ' (PRODUCT_ID, PRODUCTCATEGORY_ID, PRODUCT_PARENT_ID, PRODUCT_NAME, PRODUCT_DESCRIPTION, PRODUCT_PRICE, PRODUCT_MEASUREMENT_UNIT, PRODUCT_IMAGE_URL, PRODUCT_IS_ACTIVE, OUTLET_ID, PRODUCT_CREATED_DATE, PRODUCT_LAST_UPDATED_DATE, PRODUCT_IS_DELETED)'
        query += ' VALUES ('
        query += "UNHEX(REPLACE('" + uuidv4() + "', '-', '')), "
        if (menuData.PRODUCTCATEGORY_ID !== null) {
            let normalCategoryId = uuidParse.unparse(menuData.PRODUCTCATEGORY_ID)
            query += "UNHEX(REPLACE('" + normalCategoryId + "', '-', '')), "
        } else {
            query += "null, "
        }

        if (menuData.PRODUCT_PARENT_ID !== null) {
            let normalParentId = uuidParse.unparse(menuData.PRODUCT_PARENT_ID)
            query += "UNHEX(REPLACE('" + normalParentId + "', '-', '')), "
        } else {
            query += "null, "
        }
        query += "'" + menuData.PRODUCT_NAME + "', "
        query += "'" + menuData.PRODUCT_DESCRIPTION + "', "
        query += '' + menuData.PRODUCT_PRICE +', '
        query += '' + menuData.PRODUCT_MEASUREMENT_UNIT + ', '
        query += "'" + menuData.PRODUCT_IMAGE_URL + "', "
        query += '' + menuData.PRODUCT_IS_ACTIVE + ', '
        query += "UNHEX(REPLACE('" + normalOutletId + "', '-', '')), "
        query += "'" + currentDate + "', "
        query += "'" + currentDate + "', "
        query += '0'
        query += ')'

        console.log(query)

        let result = await conn.query(query)
        return true

    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function copyMenu(fromOutletName, targetOutletData, menuName) {
    try {
        let result = await fetchMenu(fromOutletName, menuName)
        if (result) {
            let menuData = result[0]
            if (menuData === undefined) {
                console.log('[ERROR] No such menu does exists!!!')
                process.exit()
            } else {
                console.log('> Found ' + menuName + ' data')
                console.log(menuData)
                console.log('> Do copy menu')
                console.log('> From : ' + fromOutletName)
                console.log('> To : ' + targetOutletData.OUTLET_NAME)
                console.log('> Menu name : ' + menuData.PRODUCT_NAME)
                await insertMenu(targetOutletData, menuData)
            }
        }
    } catch(error) {
        console.log('ERROR COPY MENU', error)
        process.exit()
    }
}

async function run() {
    let result = await getTargetOutletData(m_toOutletName)
    if (result) {
        let targetOutletData = result[0]
        if (targetOutletData === undefined) {
            console.log('[ERROR] No such outlet does exists!!!')
            process.exit()
        } else {
            let _ = await copyMenu(m_fromOutletName, targetOutletData, m_menuName)
            console.log('> Copy Menu successfully done!')
            console.log('> Bye bye')
            process.exit()
        }
    }
}

run()
