/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('TRANSACTIONDETAIL', {
    TRANSACTIONDETAIL_ID: {
      type: "BINARY(16)",
      allowNull: false,
      primaryKey: true
    },
    TRANSACTION_ID: {
      type: "BINARY(16)",
      allowNull: false,
      references: {
        model: 'TRANSACTION',
        key: 'TRANSACTION_ID'
      }
    },
    PRODUCT_ID: {
      type: "BINARY(16)",
      allowNull: true
    },
    MEEBERIAN_SALESORDERDETAIL_ID: {
      type: "BINARY(16)",
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_ID: {
      type: "BINARY(16)",
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_NAME: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_GENDER: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_OCCUPATION: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_BIRTHDAY: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_ADDRESS: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_CITY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_POSTAL_CODE: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_COUNTRY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    DISCOUNT_ID: {
      type: "BINARY(16)",
      allowNull: true
    },
    PRODUCT_NAME: {
      type: DataTypes.STRING(128),
      allowNull: false
    },
    PRODUCT_PRICE: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTIONDETAIL_QUANTITY: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '1.0000'
    },
    TRANSACTIONDETAIL_SUB_TOTAL: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTIONDETAIL_DISCOUNT_VALUE: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTIONDETAIL_NOTES: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    TRANSACTIONDETAIL_STATUS: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: 'RECEIVED'
    },
    TRANSACTIONDETAIL_CANCELLED_NOTES: {
      type: DataTypes.STRING(1028),
      allowNull: true
    },
    TRANSACTIONDETAIL_MEASUREMENT_UNIT: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    TRANSACTIONDETAIL_CREATED_DATE: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    TRANSACTIONDETAIL_CREATED_BY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    TRANSACTIONDETAIL_LAST_UPDATED_DATE: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    TRANSACTIONDETAIL_LAST_UPDATED_BY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    TRANSACTIONDETAIL_IS_DELETED: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    TRANSACTIONDETAIL_DELETED_DATE: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TRANSACTIONDETAIL_DELETED_BY: {
      type: DataTypes.STRING(128),
      allowNull: true
    }
  }, {
    tableName: 'TRANSACTIONDETAIL'
  });
};
