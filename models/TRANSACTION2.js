/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('TRANSACTION2', {
    TRANSACTION_ID: {
      type: DataTypes.STRING(128),
      allowNull: false,
      primaryKey: true
    },
    OUTLET_ID: {
      type: DataTypes.STRING(128),
      allowNull: false
    },
    DINETABLE_ID: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    CUSTOMER_ID: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    CUSTOMER_NAME: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: 'GUEST'
    },
    MEEBERIAN_SALESORDER_ID: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_ID: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_NAME: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_GENDER: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_OCCUPATION: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_BIRTHDAY: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_ADDRESS: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_CITY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_POSTAL_CODE: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    MEEBERIAN_CUSTOMER_COUNTRY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    TRANSACTION_PARTY: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1'
    },
    TRANSACTION_SUB_TOTAL: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTION_CHARGE_TOTAL: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTION_ADJUSTMENT_VALUE: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTION_ROUNDING_VALUE: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTION_GRAND_TOTAL: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTION_STATUS: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: 'OPEN'
    },
    TRANSACTION_CHECK_IN_DATE: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    TRANSACTION_CHECK_OUT_DATE: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TRANSACTION_QR_CHECKIN: {
      type: DataTypes.STRING(4096),
      allowNull: true
    },
    TRANSACTION_NUMBER: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    TRANSACTION_POINT_CONVERSION: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTION_POINT_AMOUNT: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    TRANSACTION_POINT_OBTAINED_QR: {
      type: DataTypes.STRING(4096),
      allowNull: true
    },
    TRANSACTION_MODULE_TYPE: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: 'TRANSACTION'
    },
    TRANSACTION_CREATED_DATE: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    TRANSACTION_CREATED_BY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    TRANSACTION_LAST_UPDATED_DATE: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    TRANSACTION_LAST_UPDATED_BY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    TRANSACTION_IS_DELETED: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    TRANSACTION_DELETED_DATE: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TRANSACTION_DELETED_BY: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    QUICKTABLE_ID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    QUICKTABLE_NAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POINT_EXPIRED_DAY: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'TRANSACTION2',
    timestamps: false,
    underscored: true
  });
};
