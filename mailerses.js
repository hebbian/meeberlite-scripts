const path = require('path');

let aws = require('aws-sdk');
aws.config.loadFromPath(path.join(__dirname, './configs/aws.key.json'));
const ses = new aws.SES();

async function send() {
    let self = this;
    let promise = new Promise(function (resolve, reject) {
        let params = {
            Source: "fikri@meeberpos.com",
            Destination: {
                ToAddresses: ["idiotiquehebb@gmail.com", "dzulfikriarrahman1@gmail.com"]
            },
            Message: {
                Subject: {
                    Data: "halo there",
                    Charset: "UTF-8"
                },
                Body: {
                    Text: {
                        Data: "halo there",
                        Charset: "UTF-8"
                    },
                    Html: {
                        Data: "halo there",
                        Charset:  "UTF-8"
                    }
                }
            }
        };

        console.log(JSON.stringify(params))

        /*
        ses.sendEmail(params, function(err, data) {
            if(err) {
                console.log('errrro', err)
                return reject(err);
            }
            console.log('successfully send email')
            return resolve(data);
        });
        */

    });

    try {
        return await promise;
    } catch(err) {
        console.log('error', err)
    }
};

send()