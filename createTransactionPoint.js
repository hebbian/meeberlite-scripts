const axios = require('axios')
const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')
const moment = require('moment')

let config = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    },
    production: {
        host: "172.31.9.66",
        user: "replication-user",
        password: "b5d98bf6576751c24dc67f16b98ac6a0",
        database: "meeber_pos_v2_auth",
        dialect: 'mysql'
    }
}

let env = process.env.NODE_ENV || 'development'
let dbconfig = config[env]
console.log('> RUNNING IN '+env+' MODE!!')
console.log('==============================')

const conn = new Sequelize(dbconfig.database, dbconfig.user, dbconfig.password, dbconfig)

async function fetchAllTransactionWithoutPoint(from, to, limit=null, outletId=null) {
    try {
        let query = "SELECT meeber_pos_v2_transaction.`TRANSACTION`.* FROM meeber_pos_v2_transaction.`TRANSACTION` WHERE meeber_pos_v2_transaction.`TRANSACTION`.TRANSACTION_NUMBER NOT IN \
        ( SELECT meeber_pos_v2_point.POINTHISTORY.TRANSACTION_NUMBER FROM meeber_pos_v2_point.POINTHISTORY ) \
        AND meeber_pos_v2_transaction.`TRANSACTION`.TRANSACTION_CHECK_IN_DATE BETWEEN '" + from + "' AND '" + to + "'";
        if (outletId !== null) {
            query += " AND meeber_pos_v2_transaction.`TRANSACTION`.OUTLET_ID = UNHEX(REPLACE('"+ outletId +"','-',''))"
        }
        if (limit !== null) {
            query += " LIMIT " + limit
        }
        let result = await conn.query(query)
        return result
    } catch(error) {
        console.log('ERROR-fetchAllTransactionWithoutPoint', error)
        process.exit()
    }
}

async function fetchOutletData(outletId) {
    try {
        let query = "SELECT meeber_pos_v2_auth.OUTLET.* FROM meeber_pos_v2_auth.OUTLET \
        WHERE meeber_pos_v2_auth.OUTLET.OUTLET_ID = UNHEX(REPLACE('"+ outletId +"','-',''))";
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR-fetchOutletData', error)
        return null
    }
}

async function fetchOutletSettingPointExpiredDay(outletId) {
    try {
        let query = "SELECT meeber_pos_v2_library.SETTING.* FROM meeber_pos_v2_library.SETTING \
        WHERE meeber_pos_v2_library.SETTING.OUTLET_ID = UNHEX(REPLACE('"+ outletId +"','-',''))";
        let result = await conn.query(query)
        let record = result[0]
        for (let i=0; i<record.length; i++) {
            if (record[i].SETTING_KEY === 'pointExpiredDay') {
                return record[i].SETTING_VALUE
            }
        }
        return '90'
    } catch(error) {
        console.log('ERROR-fetchOutletSetting', error)
        process.exit()
    }
}

async function doInsertPointHistory(data, next) {
    try {
        let query = "INSERT INTO meeber_pos_v2_point.POINTHISTORY (POINTHISTORY_ID, OUTLET_ID, TRANSACTION_ID, BUSINESS_ID, TRANSACTION_CHECK_IN_DATE, TRANSACTION_CHECK_OUT_DATE, TRANSACTION_NUMBER, TRANSACTION_MODULE_TYPE, TRANSACTION_GRAND_TOTAL,\
            MEEBERIAN_SALESORDER_ID, POINTHISTORY_POINT_CONVERSION, POINTHISTORY_POINT_AMOUNT, POINTHISTORY_POINT_OBTAINED_QR, POINTHISTORY_CREATED_DATE, POINTHISTORY_LAST_UPDATED_DATE,\
            POINTHISTORY_IS_DELETED, POINT_EXPIRED_DAY, POINT_IN_USE, POINT_REMAIN, POINTHISTORY_EXPIRED_DATE )"
        query += " VALUES ("
        query += "UNHEX(REPLACE('"+ uuidv4() +"','-','')),",
        query += "UNHEX(REPLACE('"+ data.OUTLET_ID +"','-','')),",
        query += "UNHEX(REPLACE('"+ data.TRANSACTION_ID +"','-','')),",
        query += "UNHEX(REPLACE('"+ data.BUSINESS_ID +"','-','')),",
        query += "'" + data.TRANSACTION_CHECK_IN_DATE + "',"
        query += (data.TRANSACTION_CHECK_OUT_DATE !== null) ? "'" + data.TRANSACTION_CHECK_OUT_DATE + "'," : null + ","
        query += "'" + data.TRANSACTION_NUMBER + "',"
        query += "'" + data.TRANSACTION_MODULE_TYPE + "',"
        query += "'" + data.TRANSACTION_GRAND_TOTAL + "',"
        query += (data.MEEBERIAN_SALESORDER_ID !== null) ? "UNHEX(REPLACE('"+ data.MEEBERIAN_SALESORDER_ID +"','-',''))," : null + ","
        query += "'" + data.POINTHISTORY_POINT_CONVERSION + "',"
        query += "'" + data.POINTHISTORY_POINT_AMOUNT + "',"
        query += (data.POINTHISTORY_POINT_OBTAINED_QR !== null) ? "'" + data.POINTHISTORY_POINT_OBTAINED_QR + "'," : null + ","
        query += "'" + data.POINTHISTORY_CREATED_DATE + "',"
        query += "'" + data.POINTHISTORY_LAST_UPDATED_DATE + "',"
        query += "" + data.POINTHISTORY_IS_DELETED + ","
        query += "'" + data.POINT_EXPIRED_DAY + "',"
        query += "'" + data.POINT_IN_USE + "',"
        query += "'" + data.POINT_REMAIN + "',"
        query += "'" + data.POINTHISTORY_EXPIRED_DATE + "'"
        query += ")"

        console.log('===========================')
        console.log('data', data)
        console.log('query', query)
        console.log('===========================')

        let result = await conn.query(query)
        next()

    } catch(error) {
        console.log('ERROR - doInsertPointHistory', error)
        process.exit()
    }
}

const q = async.queue((data, cb) => {
    doInsertPointHistory(data, cb)
}, 1)

async function doRun() {
    // NAV KEDUNGDORO
    //let result = await fetchAllTransactionWithoutPoint('2019-10-02 00:00:00', '2019-10-02 23:59:59', 10, '12a07140-e945-11e8-9c13-97469dc9957e')

    let result = await fetchAllTransactionWithoutPoint('2019-10-01 00:00:00', '2019-10-04 23:59:59')
    let records = result[0]
    console.log('> Found ' + records.length + ' data')
    if (records.length == 0) {
        console.log('> Nothing to do!')
        console.log('> Bye bye!')
        process.exit()
    }

    // get outlet
    let transactions = []
    for (let i=0; i<records.length; i++) {
        let ele = records[i]
        let outletId = uuidParse.unparse(ele.OUTLET_ID)
        let outletData = await fetchOutletData(outletId)
        ele.BUSINESS_ID = outletData[0].BUSINESS_ID

        let expiredDay = await fetchOutletSettingPointExpiredDay(outletId)
        ele.POINT_EXPIRED_DAY = expiredDay

        transactions.push(ele)
    }

    // insert
    transactions.forEach(async ele => {
        let data = {
            OUTLET_ID: uuidParse.unparse(ele.OUTLET_ID),
            TRANSACTION_ID: uuidParse.unparse(ele.TRANSACTION_ID),
            BUSINESS_ID: uuidParse.unparse(ele.BUSINESS_ID),
            TRANSACTION_CHECK_IN_DATE: moment(ele.TRANSACTION_CHECK_IN_DATE).format('YYYY-MM-DD HH:mm:ss'),
            TRANSACTION_CHECK_OUT_DATE: (ele.TRANSACTION_CHECK_OUT_DATE !== null) ?  moment(ele.TRANSACTION_CHECK_OUT_DATE).format('YYYY-MM-DD HH:mm:ss') : null,
            TRANSACTION_NUMBER: ele.TRANSACTION_NUMBER,
            TRANSACTION_MODULE_TYPE: ele.TRANSACTION_MODULE_TYPE,
            TRANSACTION_GRAND_TOTAL: ele.TRANSACTION_GRAND_TOTAL,
            MEEBERIAN_SALESORDER_ID: (ele.MEEBERIAN_SALESORDER_ID !== null) ? uuidParse.unparse(ele.MEEBERIAN_SALESORDER_ID) : null,
            POINTHISTORY_POINT_CONVERSION: 0,
            POINTHISTORY_POINT_AMOUNT: ele.TRANSACTION_POINT_AMOUNT,
            POINTHISTORY_POINT_OBTAINED_QR: ele.TRANSACTION_POINT_OBTAINED_QR,
            POINTHISTORY_CREATED_DATE: moment().format('YYYY-MM-DD HH:mm:ss'),
            POINTHISTORY_LAST_UPDATED_DATE: moment().format('YYYY-MM-DD HH:mm:ss'),
            POINT_IN_USE: 0,
            POINT_REMAIN: ele.TRANSACTION_POINT_AMOUNT,
            POINT_EXPIRED_DAY: ele.POINT_EXPIRED_DAY,
            POINTHISTORY_EXPIRED_DATE: moment(ele.TRANSACTION_CHECK_IN_DATE, 'YYYY-MM-DD HH:mm:ss').add(ele.POINT_EXPIRED_DAY, 'days').format('YYYY-MM-DD HH:mm:ss'),
            POINTHISTORY_IS_DELETED: 0
        }
        q.push(data)
    })
}

q.drain = () => {
    console.log('> All insert done!')
    console.log('> Bye bye!')
    process.exit();
}

doRun()
