const axios = require('axios')
const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')

// */1 * * * * /usr/local/bin/node /Users/habibi/Developments/meeber/Meeber1.0/meeber-pos-service-core-scripts/geoloc.js >> /Users/habibi/       Developments/meeber/Meeber1.0/meeber-pos-service-core-scripts/geoloc_log

let config = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    },
    production: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    }
}

let env = process.env.NODE_ENV || 'development'
let dbconfig = config[env]
console.log('> RUNNING IN '+env+' MODE!!')
console.log('==============================')

const tableName = "OUTLET_COPY"
const conn = new Sequelize(dbconfig.database, dbconfig.user, dbconfig.password, dbconfig)

async function fethAllOutlet() {
    try {
        let query = 'SELECT * FROM ' + tableName + ' WHERE OUTLET_LATITUDE IS NULL AND OUTLET_LONGITUDE IS NULL AND OUTLET_ADDRESS IS NOT NULL AND OUTLET_ADDRESS <> ""'
        let result = await conn.query(query)
        return result
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function doGetLatLong(address) {
    let googleAPIKey = "AIzaSyBzAtOS3k090Ovxmts9Cv1_BMPtaHXe-W4"
    let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + encodeURI(address) +'&key=' + googleAPIKey
    console.log(url)
    let result = await axios.get(url)
    return result
}

async function fetchAddress(address) {
    try {
        let result = await doGetLatLong(address)
        if (result.data.status === 'OK') {
            let location = result.data.results[0].geometry.location
            return location
        }
        return null
    } catch(error) {
        console.log('ERROR fetchAddress:', error.message)
        return null
    }
}

const q = async.queue((data, cb) => {
    insertIntoTable(data, cb)
}, 1)

async function doRun() {
    let result = await fethAllOutlet()
    let records = result[0]

    // get outlet
    let outlets = []
    records.forEach(ele => {
        outlets.push(ele)
    });

    // insert
    outlets.forEach(ele => {
        let data = {
            outletId: uuidParse.unparse(ele.OUTLET_ID),
            address: ele.OUTLET_ADDRESS + ' ' + ele.OUTLET_CITY + ' ' + ele.OUTLET_PROVINCE + ' ' + ele.OUTLET_COUNTRY
        }
        q.push(data)
    })
}

async function insertIntoTable(data, next) {
    if (data.address !== '') {
        let result = await fetchAddress(data.address)
        if (result !== null) {
            data.location = result
            console.log(data)
            let lat16 = data.location.lat.toString().substring(0,16)
            let lng16 = data.location.lng.toString().substring(0,16)
            let query = "UPDATE "+tableName+" SET OUTLET_LATITUDE='"+ data.location.lat +"', OUTLET_LONGITUDE='"+ data.location.lng +"' WHERE OUTLET_ID=UNHEX(REPLACE('"+ data.outletId +"','-',''))"
            console.log(query)
            let resultQuery = await doQuery(query)
            console.log(resultQuery[0])
            console.log('======================')
        }
    }
    next()
}

async function doQuery(query) {
    try {
        let result = await conn.query(query)
        return result
    } catch(error) {
        console.log('ERROR doQuery', error.message)
        return null
    }
}

q.drain = () => {
    console.log('> All insert done!')
    console.log('> Bye bye!')
    process.exit();
}

doRun()

