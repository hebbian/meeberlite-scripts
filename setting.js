const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')

let config2 = {
    host: '43.245.184.30',
    user: 'root',
    password: 'passwordroot5758',
    database: 'meeber_pos_v2_library',
    dialect: 'mysql'
}

let config = {
    host: 'db-development.meeberpos.com',
    user: 'root',
    password: 'M33b3r2015',
    database: 'meeber_pos_v2_library',
    dialect: 'mysql'
}

const tableName = "SETTING"

const conn = new Sequelize(config.database, config.user, config.password, config)

async function query() {
    let result = await conn.query('select * from '+tableName+' group by OUTLET_ID')
    return result
}

const q = async.queue((data, cb) => {
    insertIntoTable(data, cb)
}, 1)

async function doQuery() {
    let result = await query()
    let records = result[0]

    // get outlet
    let outlets = []
    records.forEach(ele => {
        outlets.push(ele.OUTLET_ID)
    });

    // insert
    outlets.forEach(ele => {
        let v4 = uuidv4()
        let binary = uuidParse.parse(v4)

        let data = {
            SETTING_ID: uuidv4(), //uuidParse.parse(uuidv4()),
            OUTLET_ID: ele,

            //SETTING_KEY: 'pointExpiredDay',
            //SETTING_VALUE: '90',

            //SETTING_KEY: 'serviceCharge',
            //SETTING_VALUE: '5',

            //SETTING_KEY: 'enableServiceCharge',
            //SETTING_VALUE: '1',

            //SETTING_KEY: 'enableTax',
            //SETTING_VALUE: '1',

            SETTING_KEY: 'tax',
            SETTING_VALUE: '10',

            SETTING_CREATED_DATE: new Date(),
            SETTING_LAST_UPDATED_DATE: new Date(),
            SETTING_IS_DELETED: 0
        }
        q.push(data)
    })
}

function insertIntoTable(data, next) {
    let que = "INSERT INTO "+tableName+" (SETTING_ID, OUTLET_ID, SETTING_KEY, SETTING_VALUE, SETTING_CREATED_DATE, SETTING_LAST_UPDATED_DATE) VALUES( UNHEX(REPLACE(:settingId, '-', '')), :outletId, :settingKey, :settingValue, :settingCreatedDate, :settingLastUpdatedDate)"
    conn.query(que, 
    {
        replacements: {
            settingId: data.SETTING_ID,
            outletId: data.OUTLET_ID,
            settingKey: data.SETTING_KEY,
            settingValue: data.SETTING_VALUE,
            settingCreatedDate: data.SETTING_CREATED_DATE,
            settingLastUpdatedDate: data.SETTING_LAST_UPDATED_DATE
        }
    }).then((result) => {
        console.log(result)
        next()
    })
}

q.drain = () => {
    console.log('All insert done!')
    process.exit();
}

doQuery()