const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://10.240.0.2:27017';

// Database Name
const dbName = 'meeber_tenant';

// Create a new MongoClient
const client = new MongoClient(url);

// Use connect method to connect to the Server
client.connect(function(err) {
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = client.db(dbName);

  let query = {
    tenant_name:/meeber_tenant_wowedcbinus_wawgambir/,
    opendinetable_endtime:/2019-05-03/,
    opendinetable_total: null
  }
  db.collection('opendinetable').find(query).forEach((docs) => {
      console.log(docs._id)
      console.log('opendinetable_total: ' + docs.opendinetable_total)
  })

  client.close();
});