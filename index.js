const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')


let config = {
    host: '43.245.184.30',
    user: 'root',
    password: 'passwordroot5758',
    database: 'meeber_pos_v2_transaction',
    dialect: 'mysql'
}
const conn = new Sequelize(config.database, config.user, config.password, config)
const TransactionTable = conn.import('./models/TRANSACTION2')

const q = async.queue((data, cb) => {
    insertIntoTable(data, cb)
}, 1)

async function normalize() {
    let result = await conn.query('select * from TRANSACTION')
    return result
}

async function doNormalize() {
    let result = await normalize()
    let records = result[0]
    console.log('Found '+records.length+' records')
    let promises = []
    for (let i=0; i<records.length; i++) {
        let ele = records[i]

        // transactionId
        let transactionId = ele.TRANSACTION_ID
        ele.TRANSACTION_ID = uuidParse.unparse(transactionId)

        // OUTLET_ID
        if (ele.OUTLET_ID != null) {
            let outletId = ele.OUTLET_ID
            ele.OUTLET_ID = uuidParse.unparse(outletId)
        }

        // CUSTOMER_ID:
        if (ele.CUSTOMER_ID != null) {
            let customerId = ele.CUSTOMER_ID
            ele.CUSTOMER_ID = uuidParse.unparse(customerId)
        }

        // MEEBERIAN_SALESORDER_ID::
        if (ele.MEEBERIAN_SALESORDER_ID != null) {
            let meeberianSalesOrderId = ele.MEEBERIAN_SALESORDER_ID
            ele.MEEBERIAN_SALESORDER_ID = uuidParse.unparse(meeberianSalesOrderId)
        }

        if (ele.MEEBERIAN_CUSTOMER_ID != null) {
            let meeberianSalesOrderId = ele.MEEBERIAN_CUSTOMER_ID
            ele.MEEBERIAN_CUSTOMER_ID = uuidParse.unparse(meeberianSalesOrderId)
        }

        if (ele.QUICKTABLE_ID != null) {
            let quickTableId = ele.QUICKTABLE_ID
            ele.QUICKTABLE_ID = uuidParse.unparse(quickTableId)
        }

        if (ele.DINETABLE_ID != null) {
            let dinetableId = ele.DINETABLE_ID
            ele.DINETABLE_ID = uuidParse.unparse(dinetableId)
        }
        q.push(ele)
    }
}

function insertIntoTable(data, next) {
    TransactionTable.create(data).then((result) => {
        console.log(result)
        next()
    })
}

q.drain = () => {
    console.log('All insert done!')
}

let _ = doNormalize()