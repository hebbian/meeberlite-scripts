const axios = require('axios')
const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')
const moment = require('moment')

let config = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    },
    production: {
        host: '172.31.9.66',
        user: 'replication-user',
        password: 'b5d98bf6576751c24dc67f16b98ac6a0',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    }
}

let env = process.env.NODE_ENV || 'development'
let dbconfig = config[env]
console.log('> RUNNING IN '+env+' MODE!!')
console.log('==============================')

const conn = new Sequelize(dbconfig.database, dbconfig.user, dbconfig.password, dbconfig)
async function fetchAllDataFromCashlessPaymentMethod() {
    try {
        console.log('> get all CASHLESSPAYMENTMETHOD')
        let query = "SELECT meeber_pos_v2_library.CASHLESSPAYMENTMETHOD.* FROM meeber_pos_v2_library.CASHLESSPAYMENTMETHOD"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function fetchAllChannelForVendor(vendor) {
    try {
        let query = "SELECT * FROM meeber_pos_v2_library.CASHLESSPAYMENTDEFAULTMDR WHERE meeber_pos_v2_library.CASHLESSPAYMENTDEFAULTMDR.CASHLESSPAYMENT_DEFAULT_MDR_VENDOR='"+ vendor +"'"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function run() {
    let channels = [
        {
            'vendor': 'dana',
            'channel': [
                {
                    'name': 'creditcard',
                    'rate': '2',
                    'charge': 0
                },
                {
                    'name': 'wallet',
                    'rate': '1.5',
                    'charge': 0
                }
            ]
        },
        {
            'vendor': 'linkaja',
            'channel': [
                {
                    'name': 'wallet',
                    'rate': '1.5',
                    'charge': 0
                }
            ]
        },
        {
            'vendor': 'credit',
            'channel': [
                {
                    'name': 'creditcard',
                    'rate': '2.4',
                    'charge': 2500
                }
            ]
        }
    ]
    let result = await fetchAllDataFromCashlessPaymentMethod()
    try {
        if (result) {
            let datas = result
            for (let i=0; i<datas.length; i++) {
                let currentVendorName = datas[i].CASHLESSPAYMENTMETHOD_NAME.toLowerCase()
                let mdrChannels = await fetchAllChannelForVendor(currentVendorName)
                if (currentVendorName === 'dana') {
                    console.log(mdrChannels)
                }

                for (let k=0; k<mdrChannels.length; k++) {
                    let currentDate = moment().format('YYYY-MM-DD HH:mm:ss')
                    let normalId = uuidParse.unparse(datas[i].CASHLESSPAYMENTMETHOD_ID)

                    let query = "INSERT INTO meeber_pos_v2_library.CASHLESSPAYMENTMETHODRATECHARGE"
                    query += " (CASHLESSPAYMENTMETHODRATECHARGE_ID, CASHLESSPAYMENTMETHOD_ID, CASHLESSPAYMENTMETHODRATECHARGE_RATE, CASHLESSPAYMENTMETHODRATECHARGE_CHARGE, CASHLESSPAYMENTMETHODRATECHARGE_CHANNEL, CASHLESSPAYMENTMETHODRATECHARGE_CREATED_DATE, CASHLESSPAYMENTMETHODRATECHARGE_LAST_UPDATED_DATE, CASHLESSPAYMENYMETHODRATECHARGE_IS_DEFAULT)"
                    query += " VALUES ("
                    query += "UNHEX(REPLACE('" + uuidv4() + "', '-', '')), " //CASHLESSPAYMENTMETHODRATECHARGE_ID
                    query += "UNHEX(REPLACE('" + normalId + "', '-', '')), " //CASHLESSPAYMENTMETHOD_ID
                    //query += "'"+ channels[j].channel[k].rate +"', " //CASHLESSPAYMENTMETHODRATECHARGE_RATE
                    //query += "'"+ channels[j].channel[k].charge +"', " //CASHLESSPAYMENTMETHODRATECHARGE_CHARGE
                    query += "null, " //CASHLESSPAYMENTMETHODRATECHARGE_RATE
                    query += "null, " //CASHLESSPAYMENTMETHODRATECHARGE_CHARGE
                    query += "'"+ mdrChannels[k].CASHLESSPAYMENT_DEFAULT_MDR_CHANNEL +"', " //CASHLESSPAYMENTMETHODRATECHARGE_CHANNEL
                    query += "'" + currentDate + "', " //CASHLESSPAYMENTMETHODRATECHARGE_CREATED_DATE
                    query += "'" + currentDate + "', " //CASHLESSPAYMENTMETHODRATECHARGED_LAST_UPDATED_DATE
                    query += "1" //CASHLESSPAYMENYMETYHODRATECHARGE_IS_DEFAULT
                    query += " )"
                    let insertresult = await conn.query(query)
                }
            }
        }
        console.log('> Insert Rate Charge successfully done!')
        console.log('> Bye bye')
        process.exit()
    } catch(error) {
        console.log('ERRROR!!!')
        console.log(error)
        process.exit()
    }
}

run()
