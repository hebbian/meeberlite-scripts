-- SELECT 
-- 	LOWER(CONCAT(
--     SUBSTR(HEX(OUTLET_ID), 1, 8), '-',
--     SUBSTR(HEX(OUTLET_ID), 9, 4), '-',
--     SUBSTR(HEX(OUTLET_ID), 13, 4), '-',
--     SUBSTR(HEX(OUTLET_ID), 17, 4), '-',
--     SUBSTR(HEX(OUTLET_ID), 21)
--   )) AS OUTLET_ID FROM meeber_pos_v2_auth.OUTLET WHERE OUTLET_NAME = 'Kalimaya Banten';

-- SELECT * FROM meeber_pos_v2_library.SETTING WHERE OUTLET_ID = UNHEX(REPLACE('50bb4460-1e14-11e9-a472-17fa72ad323c','-',''))

SELECT meeber_pos_v2_library.SETTING.*, 
LOWER(CONCAT(
    SUBSTR(HEX(meeber_pos_v2_auth.OUTLET.OUTLET_ID), 1, 8), '-',
    SUBSTR(HEX(meeber_pos_v2_auth.OUTLET.OUTLET_ID), 9, 4), '-',
    SUBSTR(HEX(meeber_pos_v2_auth.OUTLET.OUTLET_ID), 13, 4), '-',
    SUBSTR(HEX(meeber_pos_v2_auth.OUTLET.OUTLET_ID), 17, 4), '-',
    SUBSTR(HEX(meeber_pos_v2_auth.OUTLET.OUTLET_ID), 21)
  )) AS NORMAL_OUTLET_ID  
FROM meeber_pos_v2_library.SETTING 
INNER JOIN meeber_pos_v2_auth.OUTLET ON meeber_pos_v2_auth.OUTLET.OUTLET_ID = meeber_pos_v2_library.SETTING.OUTLET_ID
WHERE meeber_pos_v2_auth.OUTLET.OUTLET_NAME = 'Kalimaya Kramatjaya';