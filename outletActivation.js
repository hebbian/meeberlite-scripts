const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')
const express = require('express')
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors')
const moment = require('moment')
app.use(bodyParser.json());
app.use(cors());

let appconfig = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    },
    production: {
        host: 'IP',
        user: 'db-development.meeberpos.com',
        password: 'M33b3r2015',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    }
}
let env = process.env.NODE_ENV || 'development'
let config = appconfig[env]
const conn = new Sequelize(config.database, config.user, config.password, config)

async function query(query) {
    try {
        console.log(query)
        let result = await conn.query(query)
        return result;
    } catch(error) {
        console.log('[ERROR] '+error)
        return false
    }
}

app.get('/outlet', async (req, res) => {
    let q = 'SELECT ACCOUNT.ACCOUNT_NAME, LOGIN.LOGIN_EMAIL, OUTLET.* FROM ACCOUNT INNER JOIN LOGIN ON LOGIN.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID INNER JOIN BUSINESS ON BUSINESS.BUSINESS_ID = ACCOUNT.BUSINESS_ID INNER JOIN OUTLET ON OUTLET.BUSINESS_ID = BUSINESS.BUSINESS_ID'
    //let result = await query('SELECT * FROM OUTLET')
    let result = await query(q)
    let retval = []
    result[0].forEach(ele => {
        ele.OUTLET_ID = uuidParse.unparse(ele.OUTLET_ID)
        ele.BUSINESS_ID = uuidParse.unparse(ele.BUSINESS_ID)
        retval.push(ele)
    })
    res.json(retval)
})

app.put('/outlet/:outletId', async (req, res) => {
    let active = req.body.active
    let outletId = req.params.outletId
    let currentDate = moment().format('Y-MM-DD H:mm:ss')
    let que = "UPDATE OUTLET SET OUTLET_IS_ACTIVE = "+active+", OUTLET_ACTIVATION_DATE = '"+currentDate+"', OUTLET_LAST_UPDATED_DATE = '"+currentDate+"' WHERE OUTLET_ID = UNHEX(REPLACE('"+outletId+"', '-', ''))"
    let result = await query(que)
    res.json(result[0])
})
app.listen(5000)
