const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')
const _cliProgress = require('cli-progress')
const bar1 = new _cliProgress.Bar({}, _cliProgress.Presets.shades_classic);

let appconfig = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_transaction',
        dialect: 'mysql'
    },
    production: {
        host: 'IP',
        user: 'db-development.meeberpos.com',
        password: 'M33b3r2015',
        database: 'meeber_pos_v2_transaction',
        dialect: 'mysql'
    }
}
let env = process.env.NODE_ENV || 'development'
let config = appconfig[env]
config.logging = false
const conn = new Sequelize(config.database, config.user, config.password, config)
const tableName = "TRANSACTION"

async function getAllData() {
    try {
        let result = await conn.query("select * from "+tableName+" where MEEBERIAN_CUSTOMER_CITY = ''")
        return result
    } catch(error) {
        console.log(error)
        return null
    }
}

async function doUpdate(transactionId) {
    try {
        let result = await conn.query("UPDATE TRANSACTION set MEEBERIAN_CUSTOMER_CITY = 'Lantai 11'  WHERE TRANSACTION_ID = UNHEX(REPLACE(:transactionId, '-', ''))", 
        {
            replacements: {
                transactionId: transactionId
            }
        })
        return true
    } catch(error) {
        console.log(error)
        return false
    }
}

async function doBegin() {
    console.log('> Fetch all data')
    let allData = await getAllData()
    console.log('> Get '+ allData[0].length+' data')
    if (allData[0].length > 0) {
        console.log('> Do Update')
        let initVal = allData[0].length
        bar1.start(initVal, 0)
        for (var i=0;i<allData[0].length;i++) {
            let obj = uuidParse.unparse(allData[0][i].TRANSACTION_ID)
            let updateResult = await doUpdate(obj)
            bar1.update(i)
        }
        bar1.stop()
    }

    console.log('> All done, bye')
    process.exit()
}

doBegin()
