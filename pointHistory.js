const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')

let config = {
    host: '43.245.184.30',
    user: 'root',
    password: 'passwordroot5758',
    database: 'meeber_pos_v2_point',
    dialect: 'mysql'
}
const conn = new Sequelize(config.database, config.user, config.password, config)
const tableName = 'POINTHISTORY2'

async function query() {
    let result = await conn.query('select * from '+tableName);
    return result;
}

const q = async.queue((data, cb) => {
    updateTableData(data, cb)
}, 1)

async function doQuery() {
    let result = await query()
    let records = result[0]

    // update
    records.forEach(ele => {
        q.push(ele)
    })
}

function insertToTable(data, next) {
    let que = ""

}

function updateTableData(data, next) {
    let que = "UPDATE "+tableName+" SET POINT_EXPIRED_DAY = '90' WHERE POINTHISTORY_ID = :pointHistoryId";
    conn.query(que, 
    {
        replacements: {
            pointHistoryId: data.POINTHISTORY_ID
        }
    }).then((result) => {
        console.log(result)
        next()
    })
}

q.drain = () => {
    console.log('All update done!')
    process.exit();
}

doQuery()