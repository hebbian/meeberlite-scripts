const axios = require('axios')
const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')

let config = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    },
    production: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    }
}

let env = process.env.NODE_ENV || 'development'
let dbconfig = config[env]
console.log('> RUNNING IN '+env+' MODE!!')
console.log('==============================')

//const tableName = "OUTLET_COPY"
const tableName = "OUTLET"
const conn = new Sequelize(dbconfig.database, dbconfig.user, dbconfig.password, dbconfig)

async function fetchAllOutlet() {
    try {
        //let query = 'SELECT * FROM ' + tableName + ' WHERE OUTLET_SO_PREFIX IS NULL'
        let query = 'SELECT * FROM ' + tableName
        let result = await conn.query(query)
        return result
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function generatePrefix(data, next) {
    console.log('> Update outlet ', data.outletName)
    try {
        let existed = true
        do {
            let prefix = Math.random().toString(36).substr(2, 4).toUpperCase()
            let checkQuery = 'SELECT * FROM ' + tableName + ' WHERE OUTLET_SO_PREFIX="' + prefix + '"'
            let checkResult = await conn.query(checkQuery)
            if (checkResult[0][0] === undefined) {
                existed = false
                let query = 'UPDATE ' + tableName + ' SET OUTLET_SO_PREFIX="' + prefix + '" WHERE OUTLET_ID=UNHEX(REPLACE("'+ data.outletId +'","-",""))'
                let result = await conn.query(query)
                console.log('> Done update outlet '+ data.outletName + ' with prefix: '+ prefix)
            }
        } while(existed == true)
        next()
    } catch(error) {
        console.log('> [ERROR] Error update outlet ' + data.outletName + ' : ' + error.message)
        next()
    }
}

const q = async.queue((data, cb) => {
    generatePrefix(data, cb)
}, 1)

async function doRun() {
    let result = await fetchAllOutlet()
    let records = result[0]
    console.log('> Found ' + records.length + ' data')
    if (records.length == 0) {
        console.log('> Nothing to do!')
        console.log('> Bye bye!')
        process.exit()
    }

    // get outlet
    let outlets = []
    records.forEach(ele => {
        outlets.push(ele)
    });

    // insert
    outlets.forEach(ele => {
        let data = {
            outletId: uuidParse.unparse(ele.OUTLET_ID),
            outletName: ele.OUTLET_NAME
        }
        q.push(data)
    })
}

q.drain = () => {
    console.log('> All insert done!')
    console.log('> Bye bye!')
    process.exit();
}

doRun()
