const axios = require('axios')
const Sequelize = require('sequelize')
const uuidParse = require('uuid-parse')
const async = require('async')
const uuidv4 = require('uuid/v4')
const moment = require('moment')

let config = {
    development: {
        host: '43.245.184.30',
        user: 'root',
        password: 'passwordroot5758',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    },
    production: {
        host: '172.31.9.66',
        user: 'replication-user',
        password: 'b5d98bf6576751c24dc67f16b98ac6a0',
        database: 'meeber_pos_v2_auth',
        dialect: 'mysql'
    }
}

let m_fromOutletName = "Roti Bakar Nona Manis"
let m_toOutletName = "Eatertaint point"

let env = process.env.NODE_ENV || 'development'
let dbconfig = config[env]
console.log('> RUNNING IN '+env+' MODE!!')
console.log('==============================')

//const tableName = "OUTLET_COPY"
const tableName = "meeber_pos_v2_library.PRODUCT"
const conn = new Sequelize(dbconfig.database, dbconfig.user, dbconfig.password, dbconfig)

async function fetchAllProductCategory(outletName) {
    try {
        console.log('> Find productCategory data from outlet ' + outletName)
        let query = "SELECT meeber_pos_v2_library.PRODUCTCATEGORY.* FROM meeber_pos_v2_library.PRODUCTCATEGORY INNER JOIN meeber_pos_v2_auth.OUTLET ON meeber_pos_v2_auth.OUTLET.OUTLET_ID = meeber_pos_v2_library.PRODUCTCATEGORY.OUTLET_ID WHERE meeber_pos_v2_auth.OUTLET.OUTLET_NAME = '" + outletName + "'"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function fetchAllMenu(outletName) {
    try {
        console.log('> Find menus data from outlet ' + outletName)
        let query = "SELECT meeber_pos_v2_library.PRODUCT.*, meeber_pos_v2_library.PRODUCTCATEGORY.PRODUCTCATEGORY_NAME FROM " + tableName + " INNER JOIN meeber_pos_v2_auth.OUTLET ON meeber_pos_v2_auth.OUTLET.OUTLET_ID = " + tableName + ".OUTLET_ID INNER JOIN meeber_pos_v2_library.PRODUCTCATEGORY ON meeber_pos_v2_library.PRODUCT.PRODUCTCATEGORY_ID = meeber_pos_v2_library.PRODUCTCATEGORY.PRODUCTCATEGORY_ID WHERE meeber_pos_v2_auth.OUTLET.OUTLET_NAME = '" + outletName + "'"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function getTargetOutletData(outletName) {
    try {
        console.log('> Find outlet data ' + outletName)
        let query = "SELECT * FROM meeber_pos_v2_auth.OUTLET WHERE meeber_pos_v2_auth.OUTLET.OUTLET_NAME='" + outletName + "'"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function removeAllMenu(outletId) {
    try {
        let normalOutletId = uuidParse.unparse(outletId)
        let query = "DELETE FROM " + tableName + " WHERE OUTLET_ID=UNHEX(REPLACE('" + normalOutletId + "','-',''))"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function removeAllCategory(outletId) {
    try {
        let normalOutletId = uuidParse.unparse(outletId)
        let query = "DELETE FROM meeber_pos_v2_library.PRODUCTCATEGORY WHERE OUTLET_ID=UNHEX(REPLACE('" + normalOutletId + "','-',''))"
        let result = await conn.query(query)
        return result[0]
    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function insertProductCategory(productCategoryData) {
    try {
        console.log('> Insert product category')
        let currentDate = moment().format('YYYY-MM-DD HH:mm:ss')
        let query = "INSERT INTO meeber_pos_v2_library.PRODUCTCATEGORY"
        query += ' (PRODUCTCATEGORY_ID, OUTLET_ID, PRODUCTCATEGORY_NAME, PRODUCTCATEGORY_IS_ACTIVE, PRODUCTCATEGORY_CREATED_DATE, PRODUCTCATEGORY_LAST_UPDATED_DATE, PRODUCTCATEGORY_IS_DELETED)'
        query += ' VALUES ('
        query += "UNHEX(REPLACE('" + uuidv4() + "', '-', '')), "
        let normalOutletId = uuidParse.unparse(productCategoryData.OUTLET_ID)
        query += "UNHEX(REPLACE('" + normalOutletId + "', '-', '')), "
        query += "'" + productCategoryData.PRODUCTCATEGORY_NAME + "', "
        query += "'" + productCategoryData.PRODUCTCATEGORY_IS_ACTIVE + "', "
        query += "'" + currentDate + "', "
        query += "'" + currentDate + "', "
        query += "'0'"
        query += ')'

        let result = await conn.query(query)
        return result[0]

    } catch(error) {
        console.log('ERROR - INSERT PRODUCT CATEGORY', error)
        process.exit()
    }
}

async function insertMenu(outletData, menuData) {
    let currentDate = moment().format('YYYY-MM-DD HH:mm:ss')
    let normalOutletId = uuidParse.unparse(outletData.OUTLET_ID)

    try {
        let query = 'INSERT INTO ' + tableName
        query += ' (PRODUCT_ID, PRODUCTCATEGORY_ID, PRODUCT_PARENT_ID, PRODUCT_NAME, PRODUCT_DESCRIPTION, PRODUCT_PRICE, PRODUCT_MEASUREMENT_UNIT, PRODUCT_IMAGE_URL, PRODUCT_IS_ACTIVE, OUTLET_ID, PRODUCT_CREATED_DATE, PRODUCT_LAST_UPDATED_DATE, PRODUCT_IS_DELETED)'
        query += ' VALUES ('
        query += "UNHEX(REPLACE('" + uuidv4() + "', '-', '')), "
        if (menuData.PRODUCTCATEGORY_ID !== null) {
            let normalCategoryId = uuidParse.unparse(menuData.PRODUCTCATEGORY_ID)
            query += "UNHEX(REPLACE('" + normalCategoryId + "', '-', '')), "
        } else {
            query += "null, "
        }

        if (menuData.PRODUCT_PARENT_ID !== null) {
            let normalParentId = uuidParse.unparse(menuData.PRODUCT_PARENT_ID)
            query += "UNHEX(REPLACE('" + normalParentId + "', '-', '')), "
        } else {
            query += "null, "
        }
        let escapedProductName = menuData.PRODUCT_NAME.replace(/'/g,"\\'")
        query += "'" + escapedProductName + "', "
        console.log('#########################')
        console.log(menuData.PRODUCT_NAME)
        console.log(escapedProductName)
        console.log('#########################')
        

        let escapedProductDescription = menuData.PRODUCT_DESCRIPTION.replace(/'/g,"\\'")
        query += "'" + escapedProductDescription + "', "

        query += '' + menuData.PRODUCT_PRICE +', '
        query += '' + menuData.PRODUCT_MEASUREMENT_UNIT + ', '
        query += "'" + menuData.PRODUCT_IMAGE_URL + "', "
        query += '' + menuData.PRODUCT_IS_ACTIVE + ', '
        query += "UNHEX(REPLACE('" + normalOutletId + "', '-', '')), "
        query += "'" + currentDate + "', "
        query += "'" + currentDate + "', "
        query += '0'
        query += ')'

        console.log(query)

        let result = await conn.query(query)
        return true

    } catch(error) {
        console.log('ERROR', error)
        process.exit()
    }
}

async function copyMenu(fromOutletName, targetOutletData, targetProductCategories=[]) {
    try {
        let result = await fetchAllMenu(fromOutletName)
        if (result) {
            let menus = result
            console.log('> Found ' + menus.length + ' menu')
            for (let i=0; i<menus.length; i++) {
                let currentProductCategoryName = menus[i].PRODUCTCATEGORY_NAME
                let productCategoryId = null
                for (let j=0; j<targetProductCategories.length; j++) {
                    if (currentProductCategoryName === targetProductCategories[j].PRODUCTCATEGORY_NAME) {
                        menus[i].PRODUCTCATEGORY_ID = targetProductCategories[j].PRODUCTCATEGORY_ID
                    }
                }

                console.log('> Do copy menu')
                console.log('> Copy menu ' + menus[i].PRODUCT_NAME)
                console.log('> From : ' + fromOutletName)
                console.log('> To : ' + targetOutletData.OUTLET_NAME)
                console.log(menus[i])
                let insert = await insertMenu(targetOutletData, menus[i])
            }
        }
    } catch(error) {
        console.log('ERROR COPY MENU', error)
        process.exit()
    }
}

async function run() {
    let result = await getTargetOutletData(m_toOutletName)
    if (result) {
        let targetOutletData = result[0]
        if (targetOutletData === undefined) {
            console.log('[ERROR] No such outlet does exists!!!')
            process.exit()
        } else {

            // Remove target outlet category
            console.log('> Remove target outlet category')
            await removeAllCategory(targetOutletData.OUTLET_ID)

            // Remove target outlet menu
            console.log('> Remove target outlet menu')
            await removeAllMenu(targetOutletData.OUTLET_ID)

            // insert new category to target outlet
            console.log('> insert new category to target outlet')
            let productCategories = await fetchAllProductCategory(m_fromOutletName)
            if (productCategories.length > 0) {
                for (let i=0; i<productCategories.length; i++) {
                    let newProductCategoryData = {
                        PRODUCTCATEGORY_NAME: productCategories[i].PRODUCTCATEGORY_NAME,
                        OUTLET_ID: targetOutletData.OUTLET_ID,
                        PRODUCTCATEGORY_IS_ACTIVE: productCategories[i].PRODUCTCATEGORY_IS_ACTIVE,
                        PRODUCTCATEGORY_IS_DELETED: productCategories[i].PRODUCTCATEGORY_IS_DELETED
                    }
                    let insertedProductCategory = await insertProductCategory(newProductCategoryData)
                    console.log(insertedProductCategory)
                }
            }

            let targetProductCategories = await fetchAllProductCategory(m_toOutletName)
            let _ = await copyMenu(m_fromOutletName, targetOutletData, targetProductCategories)

            console.log('> Copy Menu successfully done!')
            console.log('> Bye bye')
            process.exit()
        }
    }
}

run()
